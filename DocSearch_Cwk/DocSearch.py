# C1634544
# I was unable to order the results by angle and not doc_list number
# The program takes much longer to run then i'd like. It does produce the corrects outputs given time. I would have it so it finds the related docs first and then creates the vectors from that. However i was unable to do this

import warnings
warnings.filterwarnings(action='ignore', category=UserWarning, module='gensim')


import gensim
import collections
from nltk.tokenize import word_tokenize
import numpy as np
import math

corpus = []

word_list = []

vector_list = []

queries = []

vector_queries = []


e = open('docs.txt' , 'r')
for line in e:
	corpus.append(line.strip())

q = open('queries.txt' , 'r')
for line in q:
	queries.append(line.strip())

doc_list = [[w.lower() for w in word_tokenize(text)] 
            for text in corpus]

query_list = [[q.lower() for q in word_tokenize(text)] 
            for text in queries]

# build dictionary
word_list = gensim.corpora.Dictionary(doc_list)

print('\nWords in dictionary:' , len(word_list))

# create list of vectors corresponding to each document
for i in range(len(doc_list)):
	vector = np.zeros(len(word_list), dtype = int)
	vector_list.append(vector)

# likewise for each query
for i in range(len(query_list)):
	vector = np.zeros(len(word_list), dtype = int)
	vector_queries.append(vector)

# fill the vectors
for wx in range(len(word_list)):
	for dx in range(len(doc_list)):
		for dwx in range(len(doc_list[dx])):
			if word_list[wx] == doc_list[dx][dwx]:
				vector_list[dx][wx] += 1
			else:
				continue

for wx in range(len(word_list)):
	for qx in range(len(query_list)):
		for qwx in range(len(query_list[qx])):
			if word_list[wx] == query_list[qx][qwx]:
				vector_queries[qx][wx] += 1
			else:
				continue

def calc_angle(x, y):
	norm_x = np.linalg.norm(x)
	norm_y = np.linalg.norm(y)
	cos_theta = np.dot(x, y) / (norm_x * norm_y)
	theta = math.degrees(math.acos(cos_theta))
	global f_theta
	f_theta = "{:.5f}".format(theta)
	return f_theta

# function to find related documents given the query
def related_search(Q):
	global related_docs 
	related_docs = []
	global related_docs_index 
	related_docs_index = []
	for dx in doc_list:
		found = set(Q).issubset(dx)
		if found == True:
			R_D = doc_list.index(dx) + 1
			related_docs.append(dx)
			related_docs_index.append(R_D)
		else:
			continue
	return related_docs_index

# output
for i in range(len(query_list)):
	print('Query: ' , queries[i])
	related_search(query_list[i])
	print('Related Documents:' , related_docs_index)
	for j in range(len(related_docs_index)):
		calc_angle(vector_queries[i], vector_list[related_docs_index[j]-1])
		print(related_docs_index[j] , f_theta)
