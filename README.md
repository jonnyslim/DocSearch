DocSearch is a script to find a word/multiple words inside very large documents.
It performs TF-IDF information retrival to determine how "important" a word is in the document
